# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# AC-01 - EX-10
# Rep com a argument un número indicatiu del número màxim de línies a mostrar. 
# El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
#
# -------------------------------------------------------
# 
# 00 Asignar constants
ERR_NUM_ARGS=1
ERR_ARG_TYPE=2

# 01 Comprobar num args
if [ $# -ne 1 ];then
  echo "ERROR $ERR_NUM_ARGS: El programa nessecita un argument"
  echo "USSAGE: $0 numLinies"
  exit $ERR_NUM_ARGS
fi

# No acabat
exit 0


# 02 Asignar variables

# 03 lectura de linies d'entrada
for read -r in 
