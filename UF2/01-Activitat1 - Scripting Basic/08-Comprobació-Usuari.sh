# /bin/bash
# @Xavier Garrido ASIX-M01
# Febrer 2024
#
# AC-01 - EX-08
# Comprobar si els usuaris existeixen en el document /etc/passwd
#
# ------------------------------------------------------------------
#
# 00 Asignar variables y constants
ERR_NUM_ARGS=1
ERR_ARG_TYPE=2

# 01 Comprobar num args
if [ $# -eq 0 ];then
  echo "ERROR $ERR_NUM_ARGS: El programa nessecita un o mes arguments"
  echo "USSAGE: $0 usuari..."
  exit $ERR_NUM_ARGS
fi

# 02 Comprbar si el usuario existe en el documento en un bucle
for user in $*
do
  grep -q "^$user:" /etc/passwd
  if [ $? -eq 0 ];then
    echo "$user"
  else
    echo "$user" >> /dev/stderr
  fi
done
exit 0
