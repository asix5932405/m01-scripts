# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
# 
# Mostrar l’entrada estàndard numerant línia a línia
# 
# -------------------------------------------------------
#
# 01 while
while read -r line 
do
  echo "$line" | cut -c 1-50 
done
exit 0

