# /bin/bash
# @Xavier Garrido ASIX-M01
# Febrer 2024
# 
# # AC-01 - EX-9
# Comprobar si els usuaris existeixen en el document /etc/passwd
#
# ------------------------------------------------------------------
#
# 01 Comprbar si el usuario existe en el documento en un bucle
while read -r user
do
  grep -q "^$user:" /etc/passwd
  if [ $? -eq 0 ];then
    echo "$user"
  else
     echo "$user" >> /dev/stderr
  fi 
done 
exit 0
