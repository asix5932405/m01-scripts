# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
# 
# Conversor de mesos a dies
# 
# -------------------------------------------------------
#
# 00 Variables i Constants
ERR_NUM_ARG=1
ERR_MES_VALID=2
comptador=1
dies=0

# 01 Comprobació num arguments
if [ $# -lt 1 ];then
  echo "ERROR $ERR_NUM_ARG: Minim un argument"
  echo "USAGE: $0 mes..."
  exit $ERR_NUM_ARG
fi
# 02 for 
for mes in $*
do
  if [ $mes -lt 1 -o $mes -gt 12 ];then
    echo "ERROR $ERR_MES_VALID: El mes $mes no esta entre els valors 1 i 12" >> /dev/stderr
  else
    case $mes in
      2) 
        dies=28 
        ;;
      4|6|9|11) 
        dies=30 
        ;;
      *) 
        dies=31 
        ;;
    esac
    echo "El mes $mes té $dies dies"
    ((comptador++))
  fi
done
exit 0
