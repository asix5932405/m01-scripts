# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
# 
# Mostrar l’entrada estàndard numerant línia a línia
# 
# -------------------------------------------------------
#
# 01 Asignar variables
comptador=1

# 02 while
while read -r line 
do 
  echo "$comptador: $line"
  ((comptador++))
done
exit 0

