# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
# 
# Fer un comptador des de zero fins al valor indicat per l’argument rebut.
# 
# -------------------------------------------------------
# Variable
comptador=$1

# 01 while
while [ $comptador -ge 0 ]
do 
  echo "$comptador"
  ((comptador--))
done
exit 0
