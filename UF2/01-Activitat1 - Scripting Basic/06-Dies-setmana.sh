# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
# 
# Exercici 06
# Clasificació dies de la setmana en festius i laborals
#
# -------------------------------------------------------
#
# 00 Variables i Constants
ERR_NUM_ARG=1
ERR_DIA_TYP=2
diesLaborables=0
diesFestius=0

# 01 validació numero arguments
if [ $# -eq 0 ];then
  echo "ERROR $ERR_NUM_ARG: Minim un argument"  >> /dev/stderr
  echo "USSAGE: $0 diaSetmana..."  >> /dev/stderr
  exit $ERR_NUM_ARG
fi

# 02 for, per tots els arguments executar un case i sumar el tipo de dia al seu grup
for diaSetmana in $*
do
  case $diaSetmana in
    'dilluns'|'dimarts'|'dimecres'|'dijous'|'divendres')
	((diesLaborables++))
	;;
    'dissabte'|'diumenge')
	((diesFestius++))
	;;
    *)
	echo "ERROR $ERR_DIA_TYP: El dia $diaSetmana no esta entre els  dies de la setmana en català" >> /dev/stderr
	echo "USSAGE: $0 diaSetmana..."  >> /dev/stderr
	;;
  esac
done

# 03 Mostrar per pantalla els resultats finals
echo "Dies Laborables: $diesLaborables"
echo "Dies Festius: $diesFestius"

exit 0
