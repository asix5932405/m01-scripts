# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# Mostrar els arguments numerant línia a línia
#
# -------------------------------------------------------
# 
# 00 Asignar constants i variables
ERR_NUM_ARGS=1
comptador=1

# 01 for
for linia in $*
do
  echo "$comptador: $linia"
  ((comptador++))
done
exit 0
