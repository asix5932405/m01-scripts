# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# AC-02 - EX-03
# Processar arguments que són matricules:
# 	a) Llistar les vàlides, del tipus: 9999-AAA.
# 	b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el
# 	   número d’errors (de no vàlides).
#
# -------------------------------------------------------
# 
# 00 Asignar constants errors
ERR_NUM_ARG=1
conMatriculesFalses=0

# 01 Validació num args
if [ $# -eq 0 ];then
  echo "ERROR $ERR_NUM_ARG: minim un argument"
  echo "US: $0 arg..."
  exit $ERR_NUM_ARG
fi

# 02 iterar per cada argument, i validar la matricula
for matricula in $*
do
  echo "$matricula" | grep -E "^[0-9]{4}-[A-Z]{3}$" 
  if [ $? -ne 0 ];then
    echo "$matricula" >> /dev/stderr
    ((conMatriculesFalses ++))
  fi
done
exit $conMatriculesFalses

