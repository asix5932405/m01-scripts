# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# AC-02 - EX-05
# Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#
# -------------------------------------------------------
#
# 00 ERRORS i variables
ERR_NUM_ARG=1

# 01 processar entrada estandar
while read -r line
do
  echo "$cont: $line" | grep "^.{1,50}$"
done
exit 0

