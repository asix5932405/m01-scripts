# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# AC-02 - EX-01.1
# 
# Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
# OK
#
# -------------------------------------------------------
#
# 00 Constants i variable
ERR_NUM_ARGS=1

# 01 Comprobació args
if [ $# -eq 0 ];then
  echo "ERROR $ERR_NUM_ARGS: El programa nessecita un o mes arguments"
  echo "USSAGE: $0 arg..."
  exit $ERR_NUM_ARGS
fi

# 02 Processar arguments
for arg in $*
do
  echo $arg | grep -Eiw ".{4,}"
done
exit 0
