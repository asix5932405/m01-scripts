# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
# 
# AC-02 - EX-02
# Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
# 
# -------------------------------------------------------
#
# 00 Asignar constants, variables i errors
ERR_NUM_ARG=1
count=0

# 01 Comprobació args
if [ $# -eq 0 ];then
  echo "ERROR $ERR_NUM_ARG: El programa nessecita un o mes arguments"
  echo "USSAGE: $0 arg..."
  exit $ERR_NUM_ARG
fi

# 02 Processar arguments
for arg in $*
do
  echo $arg | grep -Eqiw "^.{3,}$"
  if [ $? -eq 0 ];then
    ((count++))
  fi
done
echo $count
exit 0
