# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# AC-02 - EX-04
# Processar stdin mostrant per stdout les línies numerades i en majúscules.
#
# -------------------------------------------------------
#
# 00 ERRORS i variables
ERR_NUM_ARG=1
cont=1

# 01 processar entrada estandar
while read -r line
do
  echo "$cont: $line" | tr 'a-z' 'A-Z'
  ((cont ++)) 
done
exit 0

