#! /bin/bash
# @edt ASIX M01-ISO
#
# Prog dir...
# es reben n dir.
#
# -----------------------------------------------------
# 
# 00 Constants 
ERR_NUM_ARGS=1
ERR_NO_DIR=2

# 01 validar que hi ha un arg
if [ $# -eq 0 ];then
  echo "ERROR $ERR_NUM_ARGS: Num args no valid, un o mes arg"
  echo "USAGE: $0 dir..."
  exit $ERR_NUM_ARGS
fi

# 02 validar si es un directori
dir=$1
con=1

if [ ! -d $dir ];then
  echo "ERROR $ERR_NO_DIR: $dir no es un directori"
  echo "USAGE: $0 dir"
  exit $ERR_NO_DIR
fi

# 03 Llistar directori
listaNoms=$(ls $dir)
for linia in $listaNoms
do  
  if [ -d $dir/$linia ];then
    echo "$linia és un directori"
  elif [ -f $dir/$linia ];then
    echo "$linia és un regular file"
  else
    echo "$linia és una altre cosa"
  fi
done
exit 0


