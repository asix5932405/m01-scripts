#! /bin/bash
# @edt ASIX M01-ISO
#
# Prog dir
# Per a cada element dir si es dir, regular, o altra cosa.
#
# -----------------------------------------------------
# 
# 00 Constants 
ERR_NUM_ARGS=1
ERR_NO_DIR=2

# 01 validar que hi ha un arg
if [ $# -ne 1 ];then
  echo "ERROR $ERR_NUM_ARGS: Num args no valid, nomes un arg"
  echo "USAGE: $0 dir"
  exit $ERR_NUM_ARGS
fi

# 02 validar si es un directori
dir=$1
con=1

if [ ! -d $dir ];then
  echo "ERROR $ERR_NO_DIR: $dir no es un directori"
  echo "USAGE: $0 dir"
  exit $ERR_NO_DIR
fi

# 03 Llistar directori
listaNoms=$(ls $dir)
for linia in $listaNoms
do  
  if [ -d $dir/$linia ];then
    echo "$linia és un directori"
  elif [ -f $dir/$linia ];then
    echo "$linia és un regular file"
  else
    echo "$linia és una altre cosa"
  fi
done
exit 0


