# ! /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
# 
# llistar el directori rebut
# 	a) verificar rep 1 arg
# 	b) verifica arg és un directori
#
# ------------------------------------------------

# 00 Assignar errors
ERR_NUM_ARG=1
ERR_ARG_TYPE=2
# 01 Verificar només es rep un arg
if [ $# -ne 1 ]
then
  echo "Error $ERR_NUM_ARG: num arg incorrect"
  echo "Ussage: $0 directori"
  exit $ERR_NUM_ARG
fi

# 02 Verificar arg es un directori
directory=$1
if ! [ -d $directory ]
then
  echo "$directory it's not a directory"
  echo "Ussage: $0 directori"
  exit $ERR_ARG_TYPE

# 03 ostrar per pantalla el directori
else
  echo "$directory it's a directory"
fi
exit 0
