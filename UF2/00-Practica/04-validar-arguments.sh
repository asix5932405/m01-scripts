# ! /bin/bash
# @Xavier Garrido
# Validar que té exactament 2 args i mostrar-los
#
# 04-validar-arguments nom cognom
#
# --------------------------------------
#
# 01) valida que hi ha 2 args
if [ $# -ne 2 ]
then
  echo "Error: num args incorr"
  echo "Ussage: $0 nom cognom"
  exit 1
fi
# 02) xixa que esl mostra
nom=$1
cognom=$2
echo "nom: $nom"
echo "cognom: $cognom"
exit 0
