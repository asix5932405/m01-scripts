# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# Exemples ordres if
# ----------------------------------------------------

# Validació arguments o missatge d'error
if [ &# -ne 1 ]
then 
  echo "Error: num args incorr"
  echo "Ussage: $0 edat"
  exit 1
fi
# xixa
edat=$1
if [ $edat -ge 18 ]
then
  echo "edat $edat és major d'edat"
fi
exit 0
