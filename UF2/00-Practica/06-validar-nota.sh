# ! /bin/bash
# @Xavier Garrido
# Validar que té 1 arg i mostrar-los si es una nota valida entre 0 i 10
# La nota pot ser suspesa, aprobada, notable i excel·lent
# 04-validar-argument nota
#
# --------------------------------------
# 00) Configuració errors
ERR_NARGS=1
ERR_NOTA=2

# 01) valida que hi ha 1 arg
if [ $# -ne 1 ]
then
  echo "Error: num args incorr"
  echo "Ussage: $0 nota"
  exit $ERR_NARGS
fi
# 02) valida que el numero es del 1 al 10
nota=$1
if ! [ $nota -ge 0 -a $nota -le 10 ]
then 
  echo "Error: nota $1, nota just accept values in range 0 - 10"
  echo "Ussage: $0 nota"
  exit $ERR_NOTA
fi
# xixa que esl mostra
if [ $nota -lt 5 ]
then
  echo "La nota $nota està suspesa"
elif [ $nota -lt 7 ]
then
  echo "La nota $nota està aprobada"
elif [ $nota -lt 9 ]
then
  echo "La nota $nota és un notable"
else
  echo "La nota $nota és un excel·lent"
fi 
exit 0
