# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# Comprobació tipus arg
# 	a) Comprobació només un arg
# 	b) comprobació tipus arg
# ----------------------------------------------------

# 00 Errors
ERR_NUM_ARG=1

# 01 Validació només un arg
if [ $# -ne 1 ]
then
  echo "ERROR $ERR_NUM_ARG: just accept 1 argument"
  echo "USSAGE: $0 argument"
  exit $ERR_NUM_ARG
fi

# 02 Comprobació tipus arg
arg=$1
if [ -d $arg ]
then
  echo "$arg és un directori"
elif [ -f $arg ]
then
  echo "$arg és un fitxer"
else
  echo "$arg no es ni un fitxer ni un directori"
fi
exit 0
