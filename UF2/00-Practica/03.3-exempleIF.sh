# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# Exemples ordres if
# ----------------------------------------------------

# Validació arguments o missatge d'error
if [ $# -ne 1 ]
then 
  echo "Error: num args incorr"
  echo "Ussage: $0 edat"
  exit 1
fi

# xixa
edat=$1
if [ $edat -lt 18 ]
then
  echo "edat $edat és major d'edat"
elif [$edat -lt 65]
then
  echo"edat $edat és adat activa"
else
  echo "edat $edat és edat de jubilació" 
fi
exit 0
