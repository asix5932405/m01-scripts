#! /bin/bash
# exemples de funcions de disc
# @xavier_garrido abril 2024
# ------------------------------------------------------------------
function fsize(){
	login=$1
	userInfo=$(grep "^$login:" /etc/passwd)
	if [ -z "$userInfo" ];then
		echo "error:...."
		return 1
	fi
	home=$(echo $userInfo | cut -d: -f6 )
	du -sh $home
}

function loginargs(){
	if [ $# -eq 0 ];then
		echo "error:..."
		return 1
	fi
	for argument in $*
	do
		fsize $argument
	done
}

function loginfile(){
	if [ $# -ne 1 ];then
		echo "error:..."
		return 1
	fi
	if [! -f $1 ];then
		echo "error..."
		return 2
	fi
	while read -r line
	do
		fsize $line		
	done < $1 
}

function loginstdin(){
	if [ $# -ne 0 ];then
		echo "error:..."
		return 1
	fi
	while read -r line
	do
		fsize $line
	done
}

function loginboth() {
	if [ $# -eq 0 ];then
		inputToRead="/dev/stdin"
	else 
		inputToRead=$1
	fi
	while read -r line
	do
		fsize $line
	done < $inputToRead

}
