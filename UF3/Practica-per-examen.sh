# /bin/bash
# @Xavier Garrido ASIX-M01
# Gener 2024
#
# AC-02 - EX-08
# Mostrar l’entrada estàndard numerant línia a línia
#
# -------------------------------------------------------
#
#
status=0

while read -r linia 
do 
  echo "$linia" | grep -E "^[a-z,A-Z]{4}[0-9]{3}$"
  if [ $? -ne 0 ];then
    echo "$linia" > /dev/stderr
    status=3
  fi
done < $1
exit $status
