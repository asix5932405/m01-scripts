#! /bin/bash
# @xavier garrido
# funcions
#
# -----------------

function sum(){
  echo $(($1+$2))
  return 0
}

function showUserByLogin(){
  # 1) validar n args
  # 2) validar user exsist
  login=$1
  line=$(grep "^$login:" /etc/passwd)
  if [ -z "$line" ];then
    echo "error..."
    return 1
  fi 
  uid=$(echo $line | cut -d: -f3)
  gid=$(echo $line | cut -d: -f4)
  echo "login: $login"
  echo "UID: $uid" 
  echo "GID: $gid"
}

function showUsersInGroupByGid(){
  # validar exsisteix grup i mostrar gname
  gid=$1

  gname=$(grep ":$gid:[^:]*$" /etc/group | cut -d: -f1)
  if [ -z $gname ];then
    echo 'error:....'
    return 1
  fi
  echo "Group: $gname($gid)" 

  #login, uid, shell <-- gid
  grep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd | cut -d: -f1,3,7 | tr ':' ' '
}

function showAllShells(){
  shellList=$(cut -d: -f7 /etc/passwd | sort -u)
  MIN=$1 
  for shell in $shellList
  do
    num_user=$(grep ":$shell$" /etc/passwd | wc -l)
    if [ $num_user -gt $MIN ];then
      echo "SHELL $shell:"
      grep ":$shell$" /etc/passwd | cut -d: -f1,3,4 | sed 's/^/\t/'
    fi
  done
}
